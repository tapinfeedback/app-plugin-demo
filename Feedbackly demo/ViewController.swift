import UIKit
import WebKit
class ViewController: UIViewController, UIGestureRecognizerDelegate , UIWebViewDelegate, WKNavigationDelegate, WKScriptMessageHandler  {
    
    let FEEDBACKLY_CHANNEL_ID = "BJekU0gi_e";
    
    var webView: WKWebView!
    
    func getUrl() -> NSURL{
        return NSURL(string:"https://survey.feedbackly.com/surveys/\(FEEDBACKLY_CHANNEL_ID)?decorators=plugin,popup,app")!
    }
    
    func keyboardWillShow(sender: NSNotification) {
        let keyboardSize:CGSize = (sender.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size

        self.view.frame.origin.y = -keyboardSize.height // Move view 150 points upward
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    @IBAction func showSurvey(_ sender: Any) {
        unHideSurvey()
    }
    
    @IBAction func closeSurvey(_ sender: Any) {
        hideSurvey()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(sender:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(sender:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        loadSurvey()
    }
    
    func loadSurvey(){
        let pluginHeight = self.view.frame.height - (self.view.frame.height*0.60);
        
        let contentController = WKUserContentController();
        contentController.add(
            self,
            name: "callbackHandler"
        )
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        webView = WKWebView(frame: CGRect( x: 0, y: self.view.frame.height-pluginHeight, width: self.view.frame.width, height: pluginHeight), configuration: config )
        self.view.addSubview(webView)
        webView.isHidden = true;
        let req = NSURLRequest(url:getUrl() as URL)
        webView.load(req as URLRequest)
        self.webView.allowsBackForwardNavigationGestures = true
        
    }
    
    func unHideSurvey(){
        webView.isHidden = false;
    }
    
    func hideSurvey(){
        webView.isHidden = true;
        webView.evaluateJavaScript("initFromClient()", completionHandler: nil)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "callbackHandler") {
            handleMessagesFromJavaScript(message: message)
        }
    }
    
    func handleMessagesFromJavaScript(message: WKScriptMessage){
        let json = JSON.convertStringToDictionary(text: message.body as! String);
        if let action = json?["action"]{
            if(action as! String == "surveyLoaded"){
                print("survey is loaded")
            }
            if(action as! String == "surveyFinished"){
                print("survey is Finished")
                _ = Delay.setTimeout(delay: 2, block: { () -> Void in
                    self.hideSurvey()
                })

            }
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
