//
//  delay.swift
//  Tapin feedback
//
//  Created by Joonas Hamunen on 6.8.2015.
//  Copyright (c) 2015 Tapin Solutions Oy. All rights reserved.
//

import Foundation

public class Delay{
    class func setTimeout(delay:TimeInterval, block:@escaping ()->Void) -> Timer {
        return Timer.scheduledTimer(timeInterval: delay, target: BlockOperation(block: block), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }
    
    class func setInterval(interval:TimeInterval, block:@escaping ()->Void) -> Timer {
        return Timer.scheduledTimer(timeInterval: interval, target: BlockOperation(block: block), selector: #selector(Operation.main), userInfo: nil, repeats: true)
    }
}
