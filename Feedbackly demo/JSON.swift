//
//  JSON.swift
//  Tapin feedback
//
//  Created by Joonas Hamunen on 16/11/15.
//  Copyright © 2015 Tapin Solutions Oy. All rights reserved.
//

import Foundation

public class JSON{
    class func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                //print("Something went wrong")
            }
        }
        return nil
    }
}
